

import tweepy
import pandas as pd
import requests
import authentication_token
####input your credentials here
consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)


token=authentication_token.getAccessToken()


def getTweets(userId, limit=200):
        tweets = []
        try:
            tweetsObj = tweepy.Cursor( \
                    api.user_timeline, \
                    user_id=userId, \
                    exclude_replies = True \
                    ).items(limit)
            for cnt, tweet in enumerate(tweetsObj):
                if not cnt < limit:
                    break
                # print(tweet.text.replace("\n", ""))
                tweets.append(tweet)
        except tweepy.error.TweepError as et:
            print(et)
        
        return tweets 

def get_user_tweets(id, count, query):
    print("getting tweets for user: "+str(id)+" with count = "+str(count)+" and query = "+str(query))
    params = {"user_id" : id,
              "q" : query,
              "count" : count,
              "tweet_mode": "extended"}             
    header = {"Authorization" : "Bearer {}".format(token), 'Accept-Encoding': 'gzip'}
    r = requests.get("https://api.twitter.com/1.1/statuses/user_timeline.json", params=params, headers=header)
    # return tweepy.Cursor(api.user_timeline, screen_name=screen_name).items()
    return r.json()

def getUserInfo(id):
	params = {"id":id}
	header = {"Authorization" : "Bearer {}".format(token), 'Accept-Encoding': 'gzip'}
	r = requests.get("https://api.twitter.com/1.1/users/show.json",
	             params = params,
	             headers = header)
	return r.json()


def getFollowerIds(userId, limit=100):
        followerIds = []
        try:
            followers = tweepy.Cursor(\
                    api.followers_ids,\
                    user_id = userId, \
                    cursor = -1).items()
            for cnt, follower in enumerate(followers):
                if not cnt < limit:
                    break
                followerIds.append(follower)
        except tweepy.error.TweepError as et:
            print(et)
            return []
        return followerIds



def getFriendIds(userId, limit=100):
    print("getting friends for user = "+userId)
    friendIds = []
    try:
        friends = tweepy.Cursor(
                api.friends_ids,
                user_id = userId, 
                cursor = -1
                ).items()
        for cnt, friend in enumerate(friends):
            if not cnt < limit:
                break
            friendIds.append(friend)
        return friendIds
    except tweepy.error.TweepError as et:
        print(et)
        return [] 