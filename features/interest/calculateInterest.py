import csv

participant=True
input_directory = 'non_participants_result'
output_file = 'nonparticipants_interests.tsv'
user_type="general_users"
separator = "\t"

sports_file = 'sport-tags.txt'
if participant:
    user_type="ucl_users"
    input_directory = 'participants_result'
    output_file = 'participants_interests.tsv'

input_file = '../../data/'+user_type+'.tsv'


CSV_file_in = open(input_file,'r')
sports_file_in = open(sports_file,'r')
CSV_file_out = open(output_file,'w')

def isSportRelated(userTweetCSV):
    userTweetCSV = userTweetCSV.lower()
    tags = userTweetCSV.split(',')
    flag = False
    for tag in tags:
        if tag in sportTags:
            flag = True
            break
    return flag


users = CSV_file_in.readlines()
CSV_file_in.close()

sportTagLines = sports_file_in.readlines()
sports_file_in.close()

userIds = []
sportTags = []

for tagLine in sportTagLines:
    tagLine = tagLine.strip('\n')
    tagLine = tagLine.lower()
    sportTags.append(tagLine)

rows = csv.reader(users, delimiter=separator, quotechar='"')
for row in rows:
    userIds.append(row[0])

for index, userId in enumerate(userIds):
    userFile = open(input_directory + "/" + userId, 'r')
    userTweets = userFile.readlines()

    percentInterest = 0.0
    totalTweets = len(userTweets)
    sportRelated = 0

    if totalTweets > 0:
        for userTweet in userTweets:
            userTweet = userTweet.strip('\n')
            if isSportRelated(userTweet):
                sportRelated = sportRelated + 1
        percentInterest = round((sportRelated / totalTweets) * 100 , 2)
    print(userId + separator + str(percentInterest))
    CSV_file_out.write(userId + separator + str(percentInterest) + "\n")
    userFile.close()
CSV_file_out.close()
