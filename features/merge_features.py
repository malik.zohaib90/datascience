import csv


separator = "\t"
corpus = 'corpus-new.tsv'


favorites_feature_file = 'favorites/Favorites_Feature_CSV/favorites_feature_file_participants.tsv'
followers_feature_file = 'followers/results/partitipant_followers_percentage.tsv'
friends_feature_file = 'friends/results/partitipant_friends_percentage.tsv'
genders_feature_file = 'gender/results/partitipant_gender.tsv'
geolocation_feature_file = 'geolocation/countries/ucl.tsv'
interest_feature_file = 'interest/participants_interests.tsv'


#read favorites extracted features
TSV_file_in = open(favorites_feature_file,'r')
favorites = TSV_file_in.readlines()
TSV_file_in.close()
#read followers extracted features
TSV_file_in = open(followers_feature_file,'r')
followers = TSV_file_in.readlines()
TSV_file_in.close()
#read friends extracted features
TSV_file_in = open(friends_feature_file,'r')
friends = TSV_file_in.readlines()
TSV_file_in.close()
#read genders extracted features
TSV_file_in = open(genders_feature_file,'r')
genders = TSV_file_in.readlines()
TSV_file_in.close()
#read locations extracted features
TSV_file_in = open(geolocation_feature_file,'r')
locations = TSV_file_in.readlines()
TSV_file_in.close()
#read interests extracted features
TSV_file_in = open(interest_feature_file,'r')
interests = TSV_file_in.readlines()
TSV_file_in.close()



CSV_file_out = open(corpus,'w')



CSV_file_out.write('location' + separator + 'gender' + separator + 'followers%' + separator + 'friends%' + separator + 'favorites' + separator + 'interest%' + separator + 'label' +"\n")

counter = -1
for l, g, fo, fr, fa, intrst in zip(locations, genders, followers, friends, favorites, interests):
    counter = counter + 1
    if counter == 0:
        continue
    CSV_file_out.write(str(l.replace('\n',''))+separator+str(g.split('\t')[1].replace('\n',''))+separator+str(fo.split('\t')[1].replace('\n',''))+separator+str(fr.split('\t')[1].replace('\n',''))+separator+str(fa.split('\t')[1].replace('\n',''))+separator+str(intrst.split('\t')[1].replace('\n',''))+separator+'participant'+'\n')


#Do the same for general users

favorites_feature_file = 'favorites/Favorites_Feature_CSV/favorites_feature_file_non_participants.tsv'
followers_feature_file = 'followers/results/non_partitipant_followers_percentage.tsv'
friends_feature_file = 'friends/results/non_partitipant_friends_percentage.tsv'
genders_feature_file = 'gender/results/non_partitipant_gender.tsv'
geolocation_feature_file = 'geolocation/countries/general.tsv'
interest_feature_file = 'interest/nonparticipants_interests.tsv'

#read favorites extracted features
TSV_file_in = open(favorites_feature_file,'r')
favorites = TSV_file_in.readlines()
TSV_file_in.close()
#read followers extracted features
TSV_file_in = open(followers_feature_file,'r')
followers = TSV_file_in.readlines()
TSV_file_in.close()
#read friends extracted features
TSV_file_in = open(friends_feature_file,'r')
friends = TSV_file_in.readlines()
TSV_file_in.close()
#read genders extracted features
TSV_file_in = open(genders_feature_file,'r')
genders = TSV_file_in.readlines()
TSV_file_in.close()
#read locations extracted features
TSV_file_in = open(geolocation_feature_file,'r')
locations = TSV_file_in.readlines()
TSV_file_in.close()
#read interests extracted features
TSV_file_in = open(interest_feature_file,'r')
interests = TSV_file_in.readlines()
TSV_file_in.close()

counter = -1
for l, g, fo, fr, fa, intrst in zip(locations, genders, followers, friends, favorites, interests):
    counter = counter + 1
    if counter == 0:
        continue
    CSV_file_out.write(str(l.replace('\n',''))+separator+str(g.split('\t')[1].replace('\n',''))+separator+str(fo.split('\t')[1].replace('\n',''))+separator+str(fr.split('\t')[1].replace('\n',''))+separator+str(fa.split('\t')[1].replace('\n',''))+separator+str(intrst.split('\t')[1].replace('\n',''))+separator+'non-participant'+'\n')


print("Total users processed: "+str(counter*2))
CSV_file_out.close()
