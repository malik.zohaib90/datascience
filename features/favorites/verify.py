import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])

import ast
import json
import time
from datetime import datetime, timedelta
import csv
import re
import pprint
import requests
import tweepy
import authentication_token

import requests
from apis import TwitterAPIs

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

def fetchLocation(userInfo):
    # Default Latitude,Longitude for general
    location = '39.390897,-99.066067'

    if 'location' in userInfo and userInfo['location'] != '':
        data = {}

        try:
            # Make the same request we did earlier, but with the coordinates of San Francisco instead.
            parameters = { 'location': userInfo['location'], 'key': 'lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24' }
            response = requests.get("http://www.mapquestapi.com/geocoding/v1/address", params=parameters)

            # Get the response data as a python object.  Verify that it's a dictionary.
            data = response.json()
        except:
            print("Error occurred in fetch location")
            data = {}

        if 'results' in data and len(data['results']) > 0:
            location = data['results'][0]

            if 'locations' in location and len(location['locations']) > 0:
                prefferedLocation = location['locations'][0]

                if 'latLng' in prefferedLocation:
                    location = str(prefferedLocation['latLng']['lat']) + ',' + str(prefferedLocation['latLng']['lng'])
        # print(type(data))
        # print('\n')
        # print(response.status_code)

        # print(data)

    # else:
    #     location = ''

    return location

# def getUserInfo(userId):
#     location = ''
#     userInfo = TwitterAPIs.getUserInfo(userId)
#     return str(userInfo)

def tweepyUserInfo(userId):
    try:
        return api.get_user(userId)._json
    except:
        print("Error Occurre in get user info")
        return {}


consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)

separator = "\t"
input_user_file = '../../data/general_users.tsv'
input_userInfo_file = 'userInfoGeneral.tsv'
# input_location_file = 'result/locationUCL.tsv'
output_location_file = 'verified_general/locationGeneral.tsv'
output_user_info_file = 'verified_general/userInfoGeneral.tsv'

user_file = open(input_user_file,'r')
userIdCsv = user_file.readlines()
user_file.close()

user_info_file = open(input_userInfo_file,'r')
userInfos = user_info_file.readlines()
user_info_file.close()

# loc_file = open(input_location_file,'r')
# locations = loc_file.readlines()
# loc_file.close()

user_file_out = open(output_user_info_file, 'w')
location_file_out = open(output_location_file, 'w')

userIds = []
rows = csv.reader(userIdCsv, delimiter=separator, quotechar='"')
for row in rows:
    userIds.append(row[0])

totalUsers = len(userIds)
printProgressBar(0, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)

for index, userId in enumerate(userIds):
    printProgressBar(index + 1, totalUsers, prefix = 'Progress:', suffix = 'Complete', length = 50)
    ###########################################################
    ###         DO YOUR FANCY STUFF HERE                    ###
    ###        CSV_file_out.write("hello world" + "\n")     ###
    ###########################################################
    userInfo = userInfos[index].strip('\n')
    # location = location.strip('\n')
    # userId = userIds[index]
    
    userObject = ast.literal_eval(userInfo)
    
    if 'errors' in userObject and userObject['errors'][0]['code'] == 88:
        # print(userObject)
        userObject = tweepyUserInfo(userId)
        # userObject = ast.literal_eval(userObjectJson)

    location = fetchLocation(userObject)

    location_file_out.write(location + "\n")
    location_file_out.flush()
    user_file_out.write(str(userObject) + "\n")
    user_file_out.flush()

location_file_out.close()
user_file_out.close()
