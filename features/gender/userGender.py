#Extract genders From users
import ast
import os
import json
import csv

import nltk, re, pprint
from nltk import word_tokenize
from nltk.corpus import names
import random

participant = True

user_type="general_users"
directory = 'results'
output_file='non_partitipant_gender.tsv'

if not os.path.exists(directory):
    os.makedirs(directory)

if participant:
    user_type="ucl_users"
    output_file='partitipant_gender.tsv'


input_file = '../../data/'+user_type+'.tsv'




separator = "\t"

CSV_file_in = open(input_file,'r')
CSV_file_out = open(directory+"/"+output_file,'w')

lineString = CSV_file_in.readlines()
CSV_file_in.close()

def gender_features(word):
    return {'last_letter': word[-1]}

def trainClassifier():
    labeled_names = ([(name, 'male') for name in names.words('male.txt')] + [(name, 'female') for name in names.words('female.txt')])
    random.shuffle(labeled_names)

    featuresets = [(gender_features(n), gender) for (n, gender) in labeled_names]
    train_set, test_set = featuresets[500:], featuresets[:500]
    classifier = nltk.NaiveBayesClassifier.train(train_set)
    print("Accuracy: " + str(nltk.classify.accuracy(classifier, test_set)))
    return classifier

fullNames = []
genders = []
classifier = trainClassifier()

rows = csv.reader(lineString, delimiter=separator, quotechar='"')
for row in rows:
    # fullNames.append(row[2])
    fullName = row[2]
    firstName = fullName.split(' ', 1)[0]
    print(str(row[0])+" = "+str(row[1])+" = "+str(row[2]))
    gender = classifier.classify(gender_features(firstName))
    genders.append(gender)

    CSV_file_out.write(row[0] + separator + gender + "\n")

print("Names " + str(len(fullNames)) + " and genders " + str(len(genders)))
CSV_file_out.close()
