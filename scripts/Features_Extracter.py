import sys
sys.path.insert(0,'/Users/zohaib/Development/workspace/datascience')
import requests
from scripts import TwitterAPIs

def hasUserParticipated(id, count=100, keep_retweets=True, text_only=True, query="UCLfinal"):
    tweets = TwitterAPIs.get_user_tweets(id,20,query)
    for tweet in tweets:
    	if query in tweet["full_text"]:
    	 	return True
    return False

