# Importing essential libraries
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import train_test_split, KFold
from sklearn import metrics
from sklearn.metrics import precision_score, recall_score
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
from nltk.stem import WordNetLemmatizer
from sklearn.preprocessing import LabelEncoder
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import GridSearchCV
regressor = LinearRegression()
from collections import Counter
import matplotlib.pyplot as plt
import re

#Import Classifiers
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.tree import DecisionTreeClassifier
import string

def loadData():
    # Load data file
    data = pd.read_csv("../features/corpus.tsv", sep = "\t", index_col=False, encoding='latin-1', low_memory=False)
    df_old = DataFrame(data)

    # Fix null values in location to default value
    df = df_old.fillna('US')

    # Change column type to categories
    df['location'] = pd.Categorical(df.location)
    df['gender'] = pd.Categorical(df.gender)

    # Encode string to real numbers
    df['location'] = textEncoding(df['location'])
    df['gender'] = textEncoding(df['gender'])
    df['label'] = textEncoding(df['label'])

    # Split data frame to separate labels
    dfs = np.split(df, [6], axis=1)

    # Convert label dataframe to numpy array
    dfs[1] = dfs[1].values.ravel()

    # dfs[0] are feature vectors, dfs[1] are labels
    return dfs[0], dfs[1]

def textEncoding(y):
    labelEncoder = LabelEncoder()
    y_encoded = labelEncoder.fit_transform(y)
    return y_encoded

def splitTestTrain(X_vec, y_encoded, testSize):
    X_train, X_test, y_train, y_test = train_test_split(X_vec, y_encoded, 
													test_size=testSize, random_state=0, shuffle=True)
    return X_train, X_test, y_train, y_test
 


def applyNaiveBayesClassifierTuning(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    clf = MultinomialNB()
    
    # Hyperparameters: {'alpha': 1.0, 'class_prior': None, 'fit_prior': True}
    parameters = { 'alpha': [1.0, 2.0], 'fit_prior': [False, True]}

    gs_clf = GridSearchCV(clf, parameters, cv=3)
    
    # model_accuracies = cross_val_score(estimator=gs_clf, 
    #                                    X=X_train, y=y_train, cv=10)

    # print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    # print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    gs_clf.fit(X_train, y_train)
    # print(gs_clf.cv_results_)
    
    y_pred = gs_clf.predict(X_test)

    # print('accuracy',metrics.accuracy_score(y_test, y_pred))

    # bestClassifier = gs_clf.best_estimator_
    print("Best parameter for Multinomial Naive Bayes")

    for param_name in sorted(parameters.keys()):
        print(param_name,":", gs_clf.best_params_[param_name])

    
    # clf.fit(X_train, y_train)

    # print(clf.feature_count_)    

    # Model Testing: Multinomial Naive Bayes
    # y_pred = bestClassifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("Multinomial Naive Bayes Classifier Test Accuracy: ", test_accuracy*100)
    print("Multinomial Naive Bayes Classifier Test Precision: ", precision_mnb*100)
    print("Multinomial Naive Bayes Classifier Test Recall: ", recall_mnb*100)
    print("Multinomial Naive Bayes Classifier Test F measure: ", f_mnb*100)
    # return precision_mnb, recall_mnb, f_mnb



def applySVMClassifierTuning(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    clf = SVC()
    
    # Hyperparameters: {'alpha': 1.0, 'class_prior': None, 'fit_prior': True}

    parameters =  [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
                     'C': [1, 10, 100, 1000]},
                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]

    gs_clf = GridSearchCV(clf, parameters, cv=3)
    
    # model_accuracies = cross_val_score(estimator=gs_clf, 
    #                                    X=X_train, y=y_train, cv=10)

    # print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    # print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    gs_clf.fit(X_train, y_train)
    # print(gs_clf.cv_results_)
    
    y_pred = gs_clf.predict(X_test)

    # print('accuracy',metrics.accuracy_score(y_test, y_pred))

    # bestClassifier = gs_clf.best_estimator_
    print("Best parameter for SVM")

    for param_name in sorted(parameters.keys()):
        print(param_name,":", gs_clf.best_params_[param_name])

    
    # clf.fit(X_train, y_train)

    # print(clf.feature_count_)    

    # Model Testing: Multinomial Naive Bayes
    # y_pred = bestClassifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("SVM Classifier Test Accuracy: ", test_accuracy*100)
    print("SVM Classifier Test Precision: ", precision_mnb*100)
    print("SVM Classifier Test Recall: ", recall_mnb*100)
    print("SVM Classifier Test F measure: ", f_mnb*100)
    # return precision_mnb, recall_mnb, f_mnb



def applyRandomForestClassifierTuning(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    clf = RandomForestClassifier()
    
    # Hyperparameters: {'alpha': 1.0, 'class_prior': None, 'fit_prior': True}

    parameters =  {"max_depth": [3, None],
              "max_features": [1, 3, 6],
              "min_samples_split": [2, 3, 10],
              "min_samples_leaf": [1, 3, 10],
              "bootstrap": [True, False],
              "criterion": ["gini", "entropy"]}

    gs_clf = GridSearchCV(clf, parameters, cv=3)
    
    # model_accuracies = cross_val_score(estimator=gs_clf, 
    #                                    X=X_train, y=y_train, cv=10)

    # print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    # print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    gs_clf.fit(X_train, y_train)
    # print(gs_clf.cv_results_)
    
    y_pred = gs_clf.predict(X_test)

    # print('accuracy',metrics.accuracy_score(y_test, y_pred))

    # bestClassifier = gs_clf.best_estimator_
    print("Best parameter for RandomForest")
    for param_name in sorted(parameters.keys()):
        print(param_name,":", gs_clf.best_params_[param_name])

    
    # clf.fit(X_train, y_train)

    # print(clf.feature_count_)    

    # Model Testing: Multinomial Naive Bayes
    # y_pred = bestClassifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("RandomForest Classifier Test Accuracy: ", test_accuracy*100)
    print("RandomForest Classifier Test Precision: ", precision_mnb*100)
    print("RandomForest Classifier Test Recall: ", recall_mnb*100)
    print("RandomForest Classifier Test F measure: ", f_mnb*100)
    # return precision_mnb, recall_mnb, f_mnb



def applyDecisionTreeClassifierTuning(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    clf = DecisionTreeClassifier()
    
    # Hyperparameters: {'alpha': 1.0, 'class_prior': None, 'fit_prior': True}

    # print (clf.get_params().keys())
    parameters =  {
                'criterion': ['gini', 'entropy'], 
                'min_samples_leaf': [1, 10, 20],
                'max_depth': [3, 6, 9, 12],
                'class_weight': [None, 'balanced'],
                'max_features': [None, 'sqrt', 'log2'],
                'random_state': [0]
             }
    gs_clf = GridSearchCV(clf, parameters, cv=3)
    
    # model_accuracies = cross_val_score(estimator=gs_clf, 
    #                                    X=X_train, y=y_train, cv=10)

    # print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    # print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    gs_clf.fit(X_train, y_train)
    # print(gs_clf.cv_results_)
    
    y_pred = gs_clf.predict(X_test)

    # print('accuracy',metrics.accuracy_score(y_test, y_pred))

    # bestClassifier = gs_clf.best_estimator_
    print("Best parameter for DecisionTree")

    for param_name in sorted(parameters.keys()):
        print(param_name,":", gs_clf.best_params_[param_name])

    
    # clf.fit(X_train, y_train)

    # print(clf.feature_count_)    

    # Model Testing: Multinomial Naive Bayes
    # y_pred = bestClassifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("DecisionTree Classifier Test Accuracy: ", test_accuracy*100)
    print("DecisionTree Classifier Test Precision: ", precision_mnb*100)
    print("DecisionTree Classifier Test Recall: ", recall_mnb*100)
    print("DecisionTree Classifier Test F measure: ", f_mnb*100)
    # return precision_mnb, recall_mnb, f_mnb



def applyNeuralNetworkClassifierTuning(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    clf = MLPClassifier(learning_rate='adaptive', learning_rate_init=1., early_stopping=True, shuffle=True)
    
    # Hyperparameters: {'alpha': 1.0, 'class_prior': None, 'fit_prior': True}

    # print (clf.get_params().keys())
    parameters = {'solver': ['lbfgs'], 'max_iter': [500,1000,1500], 'alpha': 10.0 ** -np.arange(1, 7), 'hidden_layer_sizes':np.arange(5, 12), 'random_state':[0,1,2,3,4,5,6,7,8,9]}

    # {
    #     'hidden_layer_sizes': [(7, 7), (128,), (128, 7)],
    #     'tol': [1e-2, 1e-3, 1e-4, 1e-5, 1e-6],
    #     'epsilon': [1e-3, 1e-7, 1e-8, 1e-9, 1e-8]
    # }
    # parameters =  {
    # 'learning_rate': [0.05, 0.01, 0.005, 0.001],
    # 'hidden0__units': [4, 8, 12],
    # 'hidden0__type': ["Rectifier", "Sigmoid", "Tanh"]}
    gs_clf = GridSearchCV(clf, parameters, cv=3)
    
    # model_accuracies = cross_val_score(estimator=gs_clf, 
    #                                    X=X_train, y=y_train, cv=10)

    # print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    # print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    gs_clf.fit(X_train, y_train)
    # print(gs_clf.cv_results_)
    
    y_pred = gs_clf.predict(X_test)

    # print('accuracy',metrics.accuracy_score(y_test, y_pred))

    # bestClassifier = gs_clf.best_estimator_
    print("Best parameter for NeuralNetwork")

    for param_name in sorted(parameters.keys()):
        print(param_name,":", gs_clf.best_params_[param_name])

    
    # clf.fit(X_train, y_train)

    # print(clf.feature_count_)    

    # Model Testing: Multinomial Naive Bayes
    # y_pred = bestClassifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("NeuralNetwork Classifier Test Accuracy: ", test_accuracy*100)
    print("NeuralNetwork Classifier Test Precision: ", precision_mnb*100)
    print("NeuralNetwork Classifier Test Recall: ", recall_mnb*100)
    print("NeuralNetwork Classifier Test F measure: ", f_mnb*100)
    # return precision_mnb, recall_mnb, f_mnb



def applyNaiveBayesClassifier(X_train, y_train, X_test, y_test):
    # Thanks to sklearn, let us quickly train some multinomial models
    # Model Training: Multinomial Naive Bayes
    mnb_classifier = MultinomialNB()
    model_accuracies = cross_val_score(estimator=mnb_classifier, 
                                       X=X_train, y=y_train, cv=10)

    print("\n\nMultinomial Naive Bayes Accuracies Mean", model_accuracies.mean()*100)
    print("Multinomial Naive Bayes Accuracies Standard Devision", model_accuracies.std()*100)

    mnb_classifier.fit(X_train, y_train)
    # Model Testing: Multinomial Naive Bayes
    y_pred = mnb_classifier.predict(X_test)
    # metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("Multinomial Naive Bayes Classifier Test Accuracy: ", test_accuracy*100)
    print("Multinomial Naive Bayes Classifier Test Precision: ", precision_mnb*100)
    print("Multinomial Naive Bayes Classifier Test Recall: ", recall_mnb*100)
    print("Multinomial Naive Bayes Classifier Test F measure: ", f_mnb*100)
    return precision_mnb, recall_mnb, f_mnb

def applyNeuralNetworkClassifier(X_train, y_train, X_test, y_test):
    # Model Training: Neural Network
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
    nn_classifier = clf.fit(X_train, y_train)
    y_pred = nn_classifier.predict(X_test)

    metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_mnb = precision_score(y_test, y_pred, average='macro')  
    recall_mnb = recall_score(y_test, y_pred, average='macro') 
    f_mnb = 2*(precision_mnb*recall_mnb)/(precision_mnb+recall_mnb)
    print("\n\nNeural Network Classifier Test Accuracy: ", test_accuracy*100)
    print("Neural Network Classifier Test Precision: ", precision_mnb*100)
    print("Neural Network Classifier Test Recall: ", recall_mnb*100)
    print("Neural Network Classifier Test F measure: ", f_mnb*100)

    return precision_mnb, recall_mnb, f_mnb
    
def applySVMClassifier(X_train, y_train, X_test, y_test):
    # Model Training: SVMs
    svc_classifier = SVC(kernel='linear', random_state=1)
    svc_classifier.fit(X_train, y_train)
    # model_accuracies = cross_val_score(estimator=svc_classifier, 
    #                                X=X_train, y=y_train, cv=10) 
    # print("\n\nSVCs Accuracies Mean", model_accuracies.mean()*100)
    # print("SVCs Accuracies Standard Devision", model_accuracies.std()*100)
    # Model Testing: SVMs
    y_pred = svc_classifier.predict(X_test)
    metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_SVC = precision_score(y_test, y_pred, average='macro')  
    recall_SVC = recall_score(y_test, y_pred, average='macro') 
    f_SVC = 2*(precision_SVC * recall_SVC) / (precision_SVC + recall_SVC)
    print("\n\nSVCs Test Accuracy: ", test_accuracy*100)
    print("SVCs Test Precision: ", precision_SVC*100)
    print("SVCs Test Recall: ", recall_SVC*100)
    print("SVCs Test F measure: ", f_SVC*100)
    return precision_SVC, recall_SVC, f_SVC
    
def applyRandomForestClassifier(X_train, y_train, X_test, y_test):
    # Model Training: Random Forests Classifier
    rf_classifier = RandomForestClassifier(n_estimators=100, class_weight="balanced",
                                        criterion='entropy', random_state=1)
    rf_classifier.fit(X_train, y_train)
    # model_accuracies = cross_val_score(estimator=rf_classifier, 
    #                                X=X_train, y=y_train, cv=5) 
    # print("\n\nRandom Forest's Accuracies Mean", model_accuracies.mean()*100)
    # print("Random Forest's Accuracies Standard Devision", model_accuracies.std()*100)
    # Model Testing: Random Forests Classifier
    y_pred = rf_classifier.predict(X_test)
    metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_RF = precision_score(y_test, y_pred, average='macro')  
    recall_RF = recall_score(y_test, y_pred, average='macro') 
    f_RF = 2*(precision_RF * recall_RF) / (precision_RF + recall_RF)
    print("\n\nRandom Forest's Test Accuracy: ", test_accuracy*100)
    print("Random Forest's Test Precision: ", precision_RF*100)
    print("Random Forest's Test Recall: ", recall_RF*100)
    print("Random Forest's Test F measure: ", f_RF*100)
    return precision_RF, recall_RF, f_RF
    
# def applyLogisticRegressionClassifier(X_train, y_train, X_test, y_test):
#     #Apply Logistic Regression Classifier
#     lr = LogisticRegression(penalty = 'l2', C = 1)
#     lr.fit(X_train, y_train)
#     model_accuracies = cross_val_score(estimator=lr, 
#                                    X=X_train, y=y_train, cv=5) 
#     print("\n\nLogisticRegression_classifier Accuracies Mean", model_accuracies.mean()*100)
#     print("LogisticRegression_classifier Accuracies Standard Devision", model_accuracies.std()*100)
#     y_pred = lr.predict(X_test)
#     metrics.confusion_matrix(y_test, y_pred)
#     test_accuracy = metrics.accuracy_score(y_test, y_pred)
#     precision_LR = precision_score(y_test, y_pred, average='macro')  
#     recall_LR = recall_score(y_test, y_pred, average='macro') 
#     f_LR = 2*(precision_LR * recall_LR) / (precision_LR + recall_LR)
#     print("LogisticRegression_classifier Accuracy percent:",test_accuracy *100)
#     print("LogisticRegression_classifier Precision percent:",precision_LR *100)
#     print("LogisticRegression_classifier Recall percent:",recall_LR *100)
#     print("LogisticRegression_classifier F measure:",f_LR *100)
#     return precision_LR, recall_LR, f_LR
    
def applyDecisionTreeClassifier(X_train, y_train, X_test, y_test):
    #Apply Decision Tree Classifier
    Decision_Tree_CLF = DecisionTreeClassifier(random_state=0)
    Decision_Tree_CLF.fit(X_train, y_train)
    model_accuracies = cross_val_score(estimator=Decision_Tree_CLF, 
                                   X=X_train, y=y_train, cv=5) 
    print("\n\nDecision Tree Classifier Accuracies Mean", model_accuracies.mean()*100)
    print("Decision Tree Classifier Accuracies Standard Devision", model_accuracies.std()*100)
    y_pred = Decision_Tree_CLF.predict(X_test)
    metrics.confusion_matrix(y_test, y_pred)
    test_accuracy = metrics.accuracy_score(y_test, y_pred)
    precision_DT = precision_score(y_test, y_pred, average='macro')  
    recall_DT = recall_score(y_test, y_pred, average='macro')
    f_DT = 2*(precision_DT * recall_DT) / (precision_DT + recall_DT)
    print("Decision Tree Classifier Accuracy percent:",test_accuracy *100)
    print("Decision Tree Classifier Precision percent:",precision_DT *100)
    print("Decision Tree Classifier Recall percent:",recall_DT *100)
    print("Decision Tree Classifier Recall F measure:",f_DT *100)
    return precision_DT, recall_DT, f_DT
    
def plotLabels(y):
    #Encoding y
    y_encoded = textEncoding(y)
    #Count Labels and plot them
    y_count = Counter(y_encoded)
    key = y_count.keys()
    df = pd.DataFrame(y_count,index=key)
    df.drop(df.columns[1:], inplace=True)
    df.plot(kind='bar')

    plt.show()
    
