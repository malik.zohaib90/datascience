import requests
import base64

import authentication_token
token=authentication_token.getAccessToken()

tsvFile = open("../data/users_info.tsv", 'w')

params = {"screen_name" : "twitterapi"}             
header = {"Authorization" : "Bearer {}".format(token),
          'Accept-Encoding': 'gzip',}

r = requests.get("https://api.twitter.com/1.1/users/show.json",
             params = params,
             headers = header)
print(r.json())
tsvFile.write(str(r.json()))
tsvFile.close()

# for tweet in r.json():

#     txt = tweet['full_text']
#     isOriginal = True

#     if "retweeted_status" in tweet:
#         txt = tweet["retweeted_status"]["full_text"]
#         isOriginal = False

#     print(tweet["created_at"],"\n",txt,"\n",isOriginal,"\n")