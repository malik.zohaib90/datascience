import sys
import os
sys.path.insert(0,os.environ['DATASCIENCE'])
import tweepy
import pandas as pd
####input your credentials here
import authentication_token
# Consumer keys and access tokens, used for OAuth
consumer_key = authentication_token.client_id
consumer_secret = authentication_token.client_secret
access_token = authentication_token.user_token
access_token_secret = authentication_token.user_token_secret

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth,wait_on_rate_limit=True)
#####United Airlines
# Open/Create a file to append data
topic = "UCLfinal"
tsvFile = open("../data/"+topic+".tsv", 'w')
textFile = open("../data/"+topic+".txt", 'w')

count = 0
for tweet in tweepy.Cursor(api.search,q='#'+topic,count=100,
                           lang="en",
                           since="2018-04-26").items():
    # print (tweet.created_at, tweet.text)
    count = count + 1
    if(count%100) == 0 :
    	print ('Downloaded Tweets: '+str(count))
    tsvFile.write(str(tweet.created_at) +'\t'+ str(tweet.text.encode('utf-8'))+'\n')
    textFile.write(str(tweet._json)+'\n')
tsvFile.close()
textFile.close()

